#include <stdio.h>
#include <sys/queue.h>

struct element {
	int key;
	TAILQ_ENTRY(element) node;
};

TAILQ_HEAD(list, element);	

int main() {
	
	struct list l1;
	printf(". Declaring list...\n");

	TAILQ_INIT(&l1);
	printf(". Initializing list...\n");

	struct element e1;
	e1.key = 1;
	printf(". Declaring element...\n");

	TAILQ_INSERT_HEAD(&l1, &e1, node);
	printf(". Pushing an element to head...\n");

	struct element *e2 = TAILQ_FIRST(&l1);
	printf(". Getting the first element of list...\n");

	printf("  . The first element is %d\n", e2->key);

	return 0;
}
