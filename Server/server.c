#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>
#include <sys/stat.h>

//if on debug mode, uncommit it
// #define DEBUG
#define MAX_MES_LEN 80

#define MAX_LINE    80


// typedef struct dataList {
//     int index;              // the index of the client
//     char *address;            // need to be adjust to match socket
//     struct dataList *pre;   // the previous list
//     struct dataList *next;  // the next list
// }dataList;


typedef struct message {
	int msockfd;
	int mlen;
	char mbuf[MAX_LINE];
} message;

/*
 * Global value
 */
int sender = 0;

// message parameter
// char message[80] ;
char fileBuffer[80] = {};
int TOTAL_MESS = 0;
struct datalist *client;
char *User[1] = {"Ernie"};


// ncurses parameter
WINDOW *chat_room, *room_border, *input_box;
int win_max_row, win_max_col;
int present_row = -1;
// char inputc[80];
static char FILENAME[] = "backupFile.txt";

FILE *backupFile ;
 
/*
 *function declare
 */
int getInputData();

/*
 * Create socket for server
 * Vicky's responsability
 */
void openSocket() {

}


/*
 * send the recieve message to all the users in the chat room
 * including the sender himself
 * the sender do not print the messege out when he entered
 * this can keep the synchronize of the chat room
 */
void sendAll() {
    
}


/*
 * if the client is new to the chat room 
 */
void registClient() {
    
}

/*
 * if the client is new, send the old data for the new client
 */
void sendOldData() {
    
}

/*
 * use socket to get message from client
 * use getInputData for testing
 */
void getClientData(message *msg) {
    getInputData(msg);
    TOTAL_MESS++;
}

/*
 * send message through socket
 * @param   addr    the sending address
 * @param   message     the message to be send
 * use getInputData for testing
 */
void sendClientData(int addr, char *message) {

}


/*
 * check if the client is new
 * return whether is new or not
 */
bool clientIsNew() {
    bool answer = true;


    return answer;
}

/*
 * get input from local user, use for testing
 */
int getInputData(message *msg) {
    move( win_max_row-1, 1);               // shouldn't be larger than terminal
    refresh();
#ifdef DEBUG
    mvwprintw(chat_room, 5,1,"run to getInputData");
#endif
    int tmp = getstr(msg->mbuf);
    mvprintw(win_max_row-1, 1,"                                                          ");
    refresh();
    sender = 0;
    return tmp;
}


/*
 * write the new message to file
 */
void writeToFile(message *msg) {
    fprintf(backupFile, "%s\n", msg->mbuf);
}

/*
 * update the backup file
 * In file:
 *      #
 *      index, message
 *            .
 *            .
 *            .
 */
void updateBackup(message *msg) {
    // renew the total number of messages
    fprintf(backupFile, "%i\n", TOTAL_MESS);
    
    // move the pointer to the last of the file by getting it's size
    struct stat st;
    if (stat(FILENAME, &st) == 0) {
        fseek ( backupFile, st.st_size, SEEK_SET );
#ifdef DEBUG
    mvwprintw(chat_room, 5,1,"run ot update Backup");
#endif
    }
    
    // store the message to file
    fprintf(backupFile, "%s:\t%s\n", User[msg->msockfd], msg->mbuf);

    // move the pointer to the very front
    fseek ( backupFile, 0, SEEK_SET );
}

/*
 * open the backup file
 */
void openFile() {
    backupFile = fopen(FILENAME, "w+");
    fprintf(backupFile, "%i\n", TOTAL_MESS);
    fseek ( backupFile, 0, SEEK_SET );
}

/*
 * close the backup file when program finished
 */
void closeFile() {
    while(fclose(backupFile)){}    
}

/*
 * read from backup file
 */
void readFile() {
    fgets( fileBuffer, 200, backupFile ); 
}

/*
 * initial display
 */
void initDisplay() {
    initscr();
    getmaxyx(stdscr, win_max_row, win_max_col);
    chat_room = newwin(win_max_row-3, win_max_col-3, 1, 1);
    room_border = newwin(win_max_row-1, win_max_col-1, 0, 0);
    input_box = newwin(1, win_max_col-1, win_max_row, 0);
    refresh();

    wborder(room_border, '|', '|', '-', '-', '+', '+', '+', '+'); 
    scrollok(chat_room, TRUE);
    wrefresh(room_border);
    wrefresh(chat_room);
    refresh();
}


/*
 * display the TUI by using ncurses
 */
void displayChat(message *msg) {
    if (present_row < win_max_row-4)
        present_row++;
    else
        scroll(chat_room);
    mvwprintw(chat_room, present_row,1,"%s:     %s", User[msg->msockfd], msg->mbuf);
    wrefresh(chat_room);
}


int main(int argc, char *argv[]) {
    message *msg = (message *) argv;
    msg->msockfd = 0;
    msg->mlen = 80;
    // message *msg={0, 80, ""};
    // dataList student;
    initDisplay();
    openFile();
#ifdef DEBUG

    printf("INIT");
    printf("Msg: %d", msg->msockfd);
    printf("Msg: %d", msg->mlen);
    printf("Msg: %s", msg->mbuf);
    mvwprintw(chat_room, 5,1,"initialed\n");
#endif

    while(true) {
        getClientData(msg);
        updateBackup(msg);
        displayChat(msg);
        
    }
    closeFile();

    // getClientData();
    // openFile();
    // updateBackup();
    // closeFile();

    return 0;
}