// #include <ncurses.h>			/* ncurses.h includes stdio.h */  
// #include <string.h> 
 
// int main()
// {
//  char mesg[]="Enter a string: ";		/* message to be appeared on the screen */
//  char str[80];
//  int row,col;				/* to store the number of rows and *
// 					 * the number of colums of the screen */
//  initscr();				/* start the curses mode */
//  getmaxyx(stdscr,row,col);		/* get the number of rows and columns */
//  mvprintw(row/2,(col-strlen(mesg))/2,"%s",mesg);
//                      		/* print the message at the center of the screen */
//  getstr(str);
//  mvprintw(LINES - 2, 0, "You Entered: %s", str);
//  getch();
//  endwin();

//  return 0;
// }


// #include <ncurses.h>

// typedef struct _win_border_struct {
// 	chtype 	ls, rs, ts, bs, 
// 	 	tl, tr, bl, br;
// }WIN_BORDER;

// typedef struct _WIN_struct {

// 	int startx, starty;
// 	int height, width;
// 	WIN_BORDER border;
// }WIN;

// void init_win_params(WIN *p_win);
// void print_win_params(WIN *p_win);
// void create_box(WIN *win, bool flag);

// int main(int argc, char *argv[])
// {	WIN win;
// 	int ch;

// 	initscr();			/* Start curses mode 		*/
// 	start_color();			/* Start the color functionality */
// 	cbreak();			/* Line buffering disabled, Pass on
// 					 * everty thing to me 		*/
// 	keypad(stdscr, TRUE);		/* I need that nifty F1 	*/
// 	noecho();
// 	init_pair(1, COLOR_CYAN, COLOR_BLACK);

// 	/* Initialize the window parameters */
// 	init_win_params(&win);
// 	print_win_params(&win);

// 	attron(COLOR_PAIR(1));
// 	printw("Press F1 to exit");
// 	refresh();
// 	attroff(COLOR_PAIR(1));
	
// 	create_box(&win, TRUE);
// 	while((ch = getch()) != KEY_F(1))
// 	{	switch(ch)
// 		{	case KEY_LEFT:
// 				create_box(&win, FALSE);
// 				--win.startx;
// 				create_box(&win, TRUE);
// 				break;
// 			case KEY_RIGHT:
// 				create_box(&win, FALSE);
// 				++win.startx;
// 				create_box(&win, TRUE);
// 				break;
// 			case KEY_UP:
// 				create_box(&win, FALSE);
// 				--win.starty;
// 				create_box(&win, TRUE);
// 				break;
// 			case KEY_DOWN:
// 				create_box(&win, FALSE);
// 				++win.starty;
// 				create_box(&win, TRUE);
// 				break;	
// 		}
// 	}
// 	endwin();			/* End curses mode		  */
// 	return 0;
// }
// void init_win_params(WIN *p_win)
// {
// 	p_win->height = 3;
// 	p_win->width = 10;
// 	p_win->starty = (LINES - p_win->height)/2;	
// 	p_win->startx = (COLS - p_win->width)/2;

// 	p_win->border.ls = '|';
// 	p_win->border.rs = '|';
// 	p_win->border.ts = '-';
// 	p_win->border.bs = '-';
// 	p_win->border.tl = '+';
// 	p_win->border.tr = '+';
// 	p_win->border.bl = '+';
// 	p_win->border.br = '+';

// }
// void print_win_params(WIN *p_win)
// {
// #ifdef _DEBUG
// 	mvprintw(25, 0, "%d %d %d %d", p_win->startx, p_win->starty, 
// 				p_win->width, p_win->height);
// 	refresh();
// #endif
// }
// void create_box(WIN *p_win, bool flag)
// {	int i, j;
// 	int x, y, w, h;

// 	x = p_win->startx;
// 	y = p_win->starty;
// 	w = p_win->width;
// 	h = p_win->height;

// 	if(flag == TRUE)
// 	{	mvaddch(y, x, p_win->border.tl);
// 		mvaddch(y, x + w, p_win->border.tr);
// 		mvaddch(y + h, x, p_win->border.bl);
// 		mvaddch(y + h, x + w, p_win->border.br);
// 		mvhline(y, x + 1, p_win->border.ts, w - 1);
// 		mvhline(y + h, x + 1, p_win->border.bs, w - 1);
// 		mvvline(y + 1, x, p_win->border.ls, h - 1);
// 		mvvline(y + 1, x + w, p_win->border.rs, h - 1);

// 	}
// 	else
// 		for(j = y; j <= y + h; ++j)
// 			for(i = x; i <= x + w; ++i)
// 				mvaddch(j, i, ' ');
				
// 	refresh();

// }



#include <ncurses.h>

int main() {
    int count = 100;
    int row, col;
    char inputc[40];
    int present_row = 0;
    WINDOW *chat_room, *room_border;

    initscr();
    getmaxyx(stdscr,row,col);		/* get the number of rows and columns */
    chat_room = newwin(row-3, col-3, 1, 1);
    room_border = newwin(row-1, col-1, 0, 0);
    refresh();
    // box(chat_room, 0 , 0);
    wborder(room_border, '|', '|', '-', '-', '+', '+', '+', '+');
    mvwprintw(chat_room, 0,1,"Hello World, %i",count);
    // int tmp = wprintw(chat_room, "Hello World, %d\n", count);
    // scrollok(stdscr, TRUE);
    scrollok(chat_room, TRUE);
    wrefresh(room_border);
    wrefresh(chat_room);
    refresh();

    // mvprintw(present_row/2,0,"wrefresh return, %i",tmp);

    while (true) {
        move(row-1, 0);               // shouldn't be larger than terminal
        refresh();

        getstr(inputc);
        mvprintw(row-1,0,"                                                          ");
        refresh();
        if (present_row < row-4)
            present_row++;
        else {
            // int i = wscrl(chat_room, 1);
            // wborder(chat_room, '|', '|', '-', '-', '+', '+', '+', '+');
            
            scroll(chat_room);
        }
        
        mvwprintw(chat_room, present_row,1,"Hello World, %s",inputc);
        wrefresh(chat_room);
        // refresh();
        
    }
    // move(row-1, 0);               // shouldn't be larger than terminal
    // refresh();

    // getstr(inputc);
    // mvprintw(row-1,0,"                             ");
    // refresh();

    // mvprintw(1,0,"Hello World, %s",inputc);
    // refresh();
    getch();
    endwin();
    return 0;
}
// gnugcc test_tui.c -lncurses


