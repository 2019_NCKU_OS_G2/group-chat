#include <stdio.h>
#include <stdlib.h> // for exit()
#include <string.h> // for bzero()
#include <unistd.h> // for close()
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

// include multi-thread API of POSIX
#include <pthread.h> 

#define SERVER_PORT 5432
#define MAX_PENDING 5
#define MAX_LINE    256

typedef struct message {
	int msockfd;
	int mlen;
	char mbuf[MAX_LINE];
} message;

void *send_msg(void *arg) {
	message *msg = (message *) arg;
	while (fgets(msg->mbuf, sizeof(msg->mbuf), stdin)) {
		msg->mbuf[MAX_LINE - 1] = '\0';
		msg->mlen = strlen(msg->mbuf) + 1;
		send(msg->msockfd, msg->mbuf, msg->mlen, 0);
	}
}

int main ()
{
	struct sockaddr_in sin;
	char               buf[MAX_LINE];
	int                len;
	int                s, new_s;

	/* build address data  structure */
	bzero((char*)&sin, sizeof(sin));
	sin.sin_family      = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port        = htons(SERVER_PORT);

	/* setup passive open */
	if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("simplex talk: socket");
		exit(1);
	}

	if ((bind(s, (struct sockaddr*)&sin, sizeof(sin))) < 0) {
		perror("simplex-talk: bind");
		exit(1);
	}

	listen(s, MAX_PENDING);

	/* wait for connection, then receive and print text */
	while(1) {
		if ((new_s = accept(s, (struct sockaddr *)&sin, &len)) < 0) {
			perror("simplex-talk: accpet");
			exit(1);
		} else if (new_s > 0) {
			break;
		}

	}

	pthread_t thrd;
	message msg;
	
	msg.msockfd = new_s;

	/* open a thread to send message to client */	
	pthread_create(&thrd, NULL, send_msg, (void*) &msg);

	/* receive and print text */
	while (1) {
		while (len = recv(new_s, buf, sizeof(buf), 0))
			fputs(buf, stdout);
	}

	close(new_s);
	pthread_join(thrd, NULL);
}
