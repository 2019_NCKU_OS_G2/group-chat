#include <stdio.h>
#include <stdlib.h> // for exit()
#include <string.h> // for strlen()
#include <unistd.h> // for close()
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ncurses.h>

// include multi-thread API of POSIX
#include <pthread.h> 

#define SERVER_PORT 8740
#define MAX_LINE    256
#define UNAME_LEN   100

/*************************** global variables *********************************/

typedef struct message {
	int msockfd;
	int mlen;
	char mbuf[MAX_LINE];
} message;

char username[UNAME_LEN] = {};

// ncurses parameter
WINDOW *chat_room, *room_border, *input_box;
int win_max_row, win_max_col;
int present_row = -1;

/********************** functions for back-up files ***************************/

/*
 * get input from local user, use for testing
 */
int getInputData(char *msg) {
    // move( win_max_row-1, 1); // shouldn't be larger than terminal
    refresh();
#ifdef DEBUG
    mvwprintw(chat_room, 5,1,"run to getInputData");
#endif
    int tmp = getstr(msg);
	for(int i = 0; i < win_max_col; i++)
    	mvprintw(win_max_row - 1, i," ");
	move( win_max_row - 1, 1);
    refresh();
    return tmp;
}


/*
 * display the TUI by using ncurses
 */
void displayChat(message *msg) {
	int orig_x, orig_y;
	getyx(stdscr, orig_y, orig_x);
	int end_not = 0;
	for(end_not = 0; end_not < sizeof(msg->mbuf); end_not++) {
		if(msg->mbuf[end_not] == '\0')
			break;
	}
	if(end_not > win_max_col-4)
		end_not = 2;
	
    if (present_row < win_max_row-4) {
        present_row++;
	}
    else {
		wscrl(chat_room, 1);
		if(end_not == 2) {
			present_row--;
			scroll(chat_room);
		}
	}
	
    mvwprintw(chat_room, present_row, 1,"%s", msg->mbuf);
	if (end_not == 2) {
		present_row++;
		if (present_row > win_max_row - 4)
			present_row = win_max_row - 4;
	}
	
    wrefresh(chat_room);
	
	move( orig_y, orig_x); // shouldn't be larger than terminal
    refresh();
}

/*
 * initial display
 */
void initDisplay() {
    initscr();
    getmaxyx(stdscr, win_max_row, win_max_col);
    chat_room = newwin(win_max_row-3, win_max_col-3, 1, 1);
    room_border = newwin(win_max_row-1, win_max_col-1, 0, 0);
    input_box = newwin(1, win_max_col-1, win_max_row, 0);
    refresh();

    wborder(room_border, '|', '|', '-', '-', '+', '+', '+', '+'); 
    scrollok(chat_room, TRUE);
    wrefresh(room_border);
    wrefresh(chat_room);
	move( win_max_row-1, 1);
    refresh();
}

/**************************** sign up function ********************************/

void sign_up(void) {
	printf(" . Please enter username: ");
	fgets(username, sizeof(username), stdin);

	// paste ": " behind the username
	int u_name_len = strlen(username);
	username[u_name_len - 1] = ':';
	username[u_name_len ] = ' ';
	username[u_name_len + 1] = '\0';
}

/**************** functions for recv message from server **********************/

void *recv_msg(void *arg) {
	message *msg = (message *) arg;
	while (1) {
		while (msg->mlen = recv(msg->msockfd, msg->mbuf, sizeof(msg->mbuf), 0))
			displayChat(msg);
			// fputs(msg->mbuf, stdout);
	}
}

/**************************** main function ***********************************/

int main (int argc, char* argv[])
{
	FILE               *fp;
	struct hostent     *hp;
	struct sockaddr_in sin;
	char               *host;
	char               buf[MAX_LINE];
	int                s;
	int                len;

	if (argc == 2) {
		host = argv[1];
		printf(" . Host Name set up success ...\n");
	}
	else {
		fprintf(stderr, "usage: multiplex-talk host\n");
		exit(1);
	}

	// sign up for the username
	sign_up();

	// translate host name into peer's IP address
	hp = gethostbyname(host);

	if (!hp) {
		fprintf(stderr, "multiplex-talk: unknown host: %s\n", host);
		exit(1);
	}
	printf(" . Host IP set up success ...\n");
	
	// build address data structure
	bzero((char*)&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	bcopy(hp->h_addr, (char*)&sin.sin_addr, hp->h_length);
	sin.sin_port = htons(SERVER_PORT);

	// active open
	if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("multiplex-talk: socket");
		exit(1);
	}
	printf(" . Socket set up success ...\n");

	// connect the server
	if (connect(s, (struct sockaddr*)&sin, sizeof(sin)) < 0) {
		perror("multiplex-talk: connect");
		close(s);
		exit(1);
	}
	printf(" . Connection success ...\n");

	pthread_t thrd;
	message msg;

	msg.msockfd = s;
	
	// open a thread to receive message and print
	pthread_create(&thrd, NULL, recv_msg, (void*) &msg);

	initDisplay();

	char tempp[UNAME_LEN-2];
	memset(tempp, 0, strlen(tempp));
	char tmp[MAX_LINE]={};
	strncpy(tempp, username, strlen(username)-2);
	sprintf(tmp, "%s enter the chatroom !", tempp);
	send(s, tmp, strlen(tmp), 0);

	// main loop: get and send lines of text
	while (!getInputData(buf)) {
		
		len = strlen(buf) + 1 + strlen(username);
		
		// copy username as prefix
		char temp[UNAME_LEN + MAX_LINE];

		memset(temp, 0, strlen(temp));

		strncpy(temp, username, strlen(username));
		strncat(temp, buf, MAX_LINE);
		strncpy(buf, temp, MAX_LINE);

		send(s, buf, len, 0);

		// clean the array
		memset(buf, 0, strlen(buf));
		memset(temp, 0, strlen(temp));
	}

	pthread_join(thrd, NULL);
}
