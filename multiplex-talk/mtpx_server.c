#include <stdio.h>
#include <stdlib.h> // for exit()
#include <string.h> // for bzero()
#include <unistd.h> // for close()
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h> // for file attribute
#include <ncurses.h>  // for tui function
#include <netinet/in.h>
#include <netdb.h>

// include multi-thread API of POSIX
#include <pthread.h> 

#define SERVER_PORT 8740
#define MAX_PENDING 5
#define MAX_LINE    256
#define MAX_MES_LEN MAX_LINE
#define MAX_CLIENT  10 

/*************************** global variables *********************************/

// global variable for connection management
int con[MAX_CLIENT] = {};

typedef struct message {
	int msockfd;
	int mlen;
	char mbuf[MAX_LINE];
} message;

int sender = 0;

// message parameter
char frame[MAX_MES_LEN];
char fileBuffer[MAX_MES_LEN] = {};
int TOTAL_MESS = 0;
struct datalist *client;
char *User[1] = {"Ernie"};

// ncurses parameter
WINDOW *chat_room, *room_border, *input_box;
int win_max_row, win_max_col;
int present_row = -1;
// char inputc[80];
static char FILENAME[] = "backupFile.txt";

FILE *backupFile ;

// mutex parameter
pthread_mutex_t mutex;

/********************** functions for back-up files ***************************/


/*
 * update the backup file
 * In file:
 *      #
 *      index, message
 *            .
 *            .
 *            .
 */
void updateBackup() {
    // renew the total number of messages
    fseek ( backupFile, 0, SEEK_SET );
    fprintf(backupFile, "%i\n", TOTAL_MESS);
    
    // move the pointer to the last of the file by getting it's size
    struct stat st;
    if (stat(FILENAME, &st) == 0) {
        fseek ( backupFile, st.st_size, SEEK_SET );
#ifdef DEBUG
        printf("file size: %i\n", st.st_size);
#endif
    }
    
    // store the message to file
    fprintf(backupFile, "%s\n", frame);

    // move the pointer to the very front
    fseek ( backupFile, 0, SEEK_SET );
}

/*
 * open the backup file
 */
void openFile() {
    backupFile = fopen(FILENAME, "r+");
	if(backupFile == NULL) {
    	backupFile = fopen(FILENAME, "w+");
	}
    fprintf(backupFile, "%i\n", TOTAL_MESS);
    fseek ( backupFile, 0, SEEK_SET );
}

/*
 * close the backup file when program finished
 */
void closeFile() {
    while(fclose(backupFile)){}    
}

/*
 * read from backup file
 */
void readFile() {
    fgets( fileBuffer, 200, backupFile ); 
}

/*
 * initial display
 */
void initDisplay() {
    initscr();
    getmaxyx(stdscr, win_max_row, win_max_col);
    chat_room = newwin(win_max_row-3, win_max_col-3, 1, 1);
    room_border = newwin(win_max_row-1, win_max_col-1, 0, 0);
    input_box = newwin(1, win_max_col-1, win_max_row, 0);
    refresh();

    wborder(room_border, '|', '|', '-', '-', '+', '+', '+', '+'); 
    scrollok(chat_room, TRUE);
    wrefresh(room_border);
    wrefresh(chat_room);
    refresh();
}


/*
 * display the TUI by using ncurses
 */
void displayChat() {
	int end_not = 0;
	for(end_not = 0; end_not < sizeof(frame); end_not++) {
		if(frame[end_not] == '\0')
			break;
	}
	if(end_not > win_max_col - 4)
		end_not = 2;
	
    if (present_row < win_max_row - 4) {
        present_row++;
	}
    else {
		wscrl(chat_room, 1);
		if(end_not == 2) {
			present_row--;
			scroll(chat_room);
		}
	}
	
    mvwprintw(chat_room, present_row, 1,"%s", frame);
	if (end_not == 2) {
		present_row++;
		if (present_row > win_max_row - 4)
			present_row = win_max_row - 4;
	}
    wrefresh(chat_room);
}

/******************** functions for send/recv message *************************/

void send_msg(message* msg) {
		send(msg->msockfd, msg->mbuf, msg->mlen, 0);
}

void *recv_msg(void *arg) {
	message *msg = (message *) arg;
	
	while (1) {
		// memset( frame,  ' ', sizeof(frame) );
		while (msg->mlen = recv(msg->msockfd, msg->mbuf, sizeof(msg->mbuf), 0)) {
			// For debug
			// mvwprintw(chat_room, msg->msockfd+5,3,"sock fd = %d",  msg->msockfd);
			// wrefresh(chat_room);

			TOTAL_MESS++;
			
			// saving data to back-up file , lock on by mutex
			pthread_mutex_lock(&mutex);

			openFile();

			// the message sent to client, write into backup, print on screen
			char *mes= malloc(sizeof(char)*msg->mlen);

			strncpy(mes, msg->mbuf, msg->mlen);
			sprintf(frame, "%s", mes);
			// sprintf(frame, "(%i) %s", msg->msockfd, mes);

			updateBackup();
			displayChat();
			closeFile();


			// char *mes2= malloc(sizeof(char)*sizeof(frame));
			// strncpy(mes2, frame, sizeof(frame));
			// free(mes);

			message* smsg = (message*) malloc(sizeof(message));
			smsg->mlen = sizeof(frame);
			strncpy(smsg->mbuf, frame, sizeof(frame));

			// unlock mutex after close file
			pthread_mutex_unlock(&mutex);


			// send message to clients
			for (int i = 0; i < MAX_CLIENT; i++) {
				if (con[i] != 0) {
					smsg->msockfd = con[i];
					send_msg(smsg);
				}
			}
			free(smsg);
			free(mes);

			// unlock mutex after close file
			pthread_mutex_unlock(&mutex);

		}
	}
}

/**************************** main function ***********************************/

int main ()
{
	struct sockaddr_in sin;
	char               buf[MAX_LINE];
	int                len = 0; // init to avoid accept error: invaild argument
	int                s, new_s;

	// build address data  structure
	bzero((char*)&sin, sizeof(sin));
	sin.sin_family      = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port        = htons(SERVER_PORT);

	// setup passive open
	if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("multiplex-talk: socket");
		exit(1);
	}

	// avoid that the socket does not be closed from the lasting listen()
	int on = 1;
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(int)) < 0)
		perror("multiplex-talk: setsockopt failed");

	if ((bind(s, (struct sockaddr*)&sin, sizeof(sin))) < 0) {
		perror("multiplex-talk: bind");
		exit(1);
	}

	// record the number of connection
	int con_num = 0;
	
	// initial mutex
	pthread_mutex_init(&mutex, 0);

	// initial tui
	initDisplay();

	// wait for connection
	while(1) {

		// listen for connection from clients
		listen(s, MAX_PENDING);

		// accept connection
		if ((new_s = accept(s, (struct sockaddr *)&sin, &len)) < 0) {
			perror("multiplex-talk: accpet");
			exit(1);
		} else if (new_s > 0) {
			// allow connection only for MAX_CLIENT
			if (con_num < MAX_CLIENT) {
				pthread_t r_thrd;
				message *r_msg= (message*) malloc(sizeof(message));

				r_msg->msockfd = new_s;

				// open a thread to send & recv message
				pthread_create(&r_thrd, NULL, recv_msg, (void*) r_msg);

				// record the file descriptor
				con[con_num] = new_s;
				con_num += 1;
			}

			new_s = 0;
		}
	}

/*************** the code below will not be executed, however *****************/

	// delete mutex
	pthread_mutex_destroy(&mutex);

	// free the socket fd
	for (int i = 0; i < MAX_CLIENT; i++) {
		if (con[i] > 0)
			close(con[i]);
	}
}
