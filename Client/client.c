#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>


struct dataList {
    int index;              // the index of the client
    char *address;            // need to be adjust to match socket
    struct dataList *pre;   // the previous list
    struct dataList *next;  // the next list
};

/*
 * Global value
 */


/*
 * Create socket for server
 * Vicky's responsability
 */
void openSocket() {

}

/*
 * use socket to get message from client
 * use getInputData for testing
 */
void getServerData() {

}

/*
 * send message through socket
 * @param   addr    the sending address
 * @param   message     the message to be send
 * use getInputData for testing
 */
void sendData(int addr, char *message) {

}

/*
 * get input from local user, use for testing
 */
void sendInputData() {

}

/*
 * send the recieve message to all the users in the chat room
 * including the sender himself
 * the sender do not print the messege out when he entered
 * this can keep the synchronize of the chat room
 */
void sendAll() {
    
}


/*
 * if the client is new to the chat room 
 */
void regist() {
    
}




/*
 * display the TUI by using ncurses
 */
void display() {

}




int main() {


    return 0;
}
