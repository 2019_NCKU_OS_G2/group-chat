# Group Chat with CLI

A real-time group chat implemented by C.

## Compile Notice

For `pthread` usage in multi-thread C files. One should add `-pthread` option while compiling.
For instance,

```
$ gcc -pthread -o thrd_client.out thrd_client.c
```

## Packet Requirement

For `ncurses.h` in Linux system, please use following command to install `libncurses5-dev`.

```
$ sudo apt install libncurses5-dev
```

## Simplex-Talk

In `simplex-talk` directory is the example of single direction socket implementation chat room from Computer Network class.

For client, run
```
$ gcc -o client.out client.c
$ ./client.out
```

For server, run
```
$ gcc -o server.out server.c
$ ./server.out
```

## Duplex-Talk

In `duplex-talk` directory is the example of duplex socket implementation chat room, which allows the server and client to communicate at the same time by introducing multi-thread programming.

For client, run
```
$ gcc -pthread -o thrd_client.out thrd_client.c
$ ./thrd_client.out
```

For server, run
```
$ gcc -pthread -o thrd_server.out thrd_server.c
$ ./thrd_server.out
```

## Todo
- socket
- multi-thread
- main structure
	- tui
	- file format
